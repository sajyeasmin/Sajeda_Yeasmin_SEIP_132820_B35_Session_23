<?php
namespace App\BITM\SEIP132820;

use PDO;
use PDOException;

class Database
{
    public $DBH;
    public $host ="localhost";
    public $dbname= "atomic_project_b37";
    public $user= "root";
    public $password="";



    public function __construct() {
        try{
            $this-> DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            echo"Connection Successfull"."<br>";
        }
        catch (PDOException $error){
            echo $error->getMessage();

        }

    }
}